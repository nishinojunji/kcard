
$debugprint = true
$debugprint = nil

$playernum = 2
$cardnum = 24

$nilcount = 0

class Player
  def initialize
    #@hand = ["G", "T", "P", "G", "T", "P"]
    #@hand = ["G", "T", "P"]
    #@hand = ["G", "T", "P","G", "T", "P","G", "T", "P","G", "T", "P"]
    @hand = Array.new($cardnum,1)
    @bed = []
  end

  def num
    half = (@hand.size/2)-1
    half = 0 if half<0
    return half
  end

  def display
    p @hand
    p @bed
  end

  def win(p)
    @hand = @hand + @bed
    @hand = @hand + p
    @bed = []
  end

  def lose
    a = @bed
    @bed = []
    return a
  end

  def ishandnil
    $nilcount = $nilcount+1
    return true if @hand == nil
    return true if @hand == []

    $nilcount = $nilcount-1
    return false
  end

  def bed(n)
    @bed = @hand[0..n]
    @hand = @hand[n+1..-1]
    @hand = [] if @hand == nil

    print "in bed:\n" if $debugprint
    p @bed if $debugprint
    p @hand if $debugprint
  end

end

def match(ps, i, j)
  print "in match: \n" if $debugprint
  p ps[i] if $debugprint
  p ps[j] if $debugprint
  return if ps[i].ishandnil
  return if ps[j].ishandnil

  bn = [ ps[i].num, ps[j].num].min
  print "bn=", bn, "\n" if $debugprint
  ps[i].bed(bn)
  ps[j].bed(bn)
  r = rand 10
  if r<5
    ps[i].win(ps[j].lose)
  else
    ps[j].win(ps[i].lose)
  end
end


def maintest
  tt=1000
  #pnum = 20
  pnum = $playernum

  players=[]
  pnum.times{
    pl = Player.new
    players.push pl
  }

  tt.times{
    i = rand pnum
    j = rand (pnum - 1)
    j = (j+i+1) % pnum
    print i, " ", j, "\n" if $debugprint
    print "error" if i==j if $debugprint

    match(players, i, j)

    players.each{|x| p x } if $debugprint

    print "-------------------\n" if $debugprint
  }

  players.each{|x| p x } if $debugprint

  p tt - $nilcount

end

maintest
